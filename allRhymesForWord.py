#!/usr/bin/env python

import sys
from collections import Counter

from dictionary import flatten
from symbols import symbols
from alliterate import SUGGESTABLE_WORDS
from proDict import proDict
from thesaurus import thesaurus


REPLACEABLE_SOUND_TUPLES = [('S','Z'), ('AE1','AE2')]

def printAllRhymesForWord(word, numSounds ):
  print(proDict[word])
  last3Sounds = proDict[word][-numSounds:]
  print(last3Sounds)
  for w in proDict:
    for a,b in REPLACEABLE_SOUND_TUPLES:
      if ''.join(proDict[w][-numSounds:]) == ''.join(last3Sounds).replace(a,b):
        print(w.split('(')[0])
      if ''.join(proDict[w][-numSounds:]) == ''.join(last3Sounds).replace(b,a):
        print(w.split('(')[0])

if __name__ == '__main__':
    if len(sys.argv) > 1:
        numSounds = int(sys.argv[1])
        word = sys.argv[2]
        printAllRhymesForWord(word, numSounds)
    else:
        print('need more args')


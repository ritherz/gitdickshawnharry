#!/usr/bin/env python

import xml.etree.ElementTree
import re
import copy
import time
from goodDefsDict import dictionary

EXISTING_WORDS = set(dictionary.keys())
numRows = 2000000
NONE = 'none'

now = lambda *args : time.time()
TIME_SINCE_LAST_UPDATE = now()

PRONUNCIATION = '===Pronunciation==='
SYNONYMS = '===Synonyms==='
LANGUAGE = '==English=='

DEFINITION_TYPES = ['Noun', 'Verb', 'Adverb', 'Adjective', 'Pronoun', 'Determiner', 'Conjunction', 'Preposition']
PUNABLE_TYPES = [t for t in DEFINITION_TYPES if t not in ['Determiner']]


def flatten(foo):
    for x in foo:
        if hasattr(x, '__iter__'):
            for y in flatten(x):
                yield y
        else:
            yield x


def getDescChunk(desc, chunkName):
  if not desc:
    return ''
  text = desc.replace('\n','NEWLINE')
  englishText = text.split(LANGUAGE)
  if len(text.split(LANGUAGE)) < 2:
      return ''
  text = text.split(LANGUAGE)[1] #english only
  text = re.split('==[^:=]+==NEWLINE', text)[0]
  text = text.replace("NEWLINE","")

  retText = re.findall('{chunkName}(.+)'.format(chunkName=chunkName), text)
  if retText:
    return retText[0].split('===')[0]
  return ''


LANGUAGE_DIALECTS = ['US', 'USA', 'Canada', 'CA', 'CAN']
def parsePronunciations(desc):
    langDialects = '|'.join(LANGUAGE_DIALECTS)
    pronunciationLinesRaw = [flatten([pline.split(' ') for pline in re.findall('\* \{\{.*(?:%s).*\}\} (.+)' % langDialects, desc)])]
    pronunciationLines = [re.findall('''\{\{(IPA|enPR)\|(\S+)\}\}''', p) for p in pronunciationLinesRaw[0]]
    retVal = []
    for p in pronunciationLines:
        if p and len(p[0]) > 1:
            pronunciation = p[0][1].split('|')[0].lstrip('/')
            retVal.append(pronunciation)
    retVal = filter(lambda x: 'lang=' not in x, retVal)
    return set(retVal)


def getAllDefinitions(desc):
    allDefinitions = lambda text: [t.split('#')[0] for t in text.split('# ') if t.split('#')[0]]
    allDefLines = lambda desc, wordType: allDefinitions(getDescChunk(desc, wordType))
    cleanDefLines = lambda lines: [line.replace("[[","").replace("]]","") for line in lines if '{{en-' not in line and line]
    cleanerLines = lambda lines: [d.replace('}}','') for d in re.split('{{[^}}]*', lines) if d]
    noDeadLines = lambda lines: [line for line in flatten(lines) if line != '']
    completelyCleanLines = lambda text, wt: noDeadLines([cleanerLines(line) for line in cleanDefLines(allDefLines(text, '===%s===' % wt))])
    retVal = {}
    for wordType in DEFINITION_TYPES:
        defs = completelyCleanLines(desc, wordType)
        if defs:
            retVal[wordType] = defs
    return retVal


def parseSynonyms(desc):
    return re.findall('{{l\|en\|(\S+)}}',desc)


def getRawData():
    wordDict = {}
    current_word = None
    currRow = 0
    skipThisWord = False
    for event, elem in context:
        if currRow % 100000 == 0:
            global TIME_SINCE_LAST_UPDATE
            print currRow, now() - TIME_SINCE_LAST_UPDATE
            TIME_SINCE_LAST_UPDATE = now()

        currRow+=1
        if event == "end" and elem.tag.endswith( "title"):
            if elem.text != 'aardvark' and not wordDict: #skip errything until the first word
              continue
            if current_word and current_word in wordDict \
                        and (not wordDict.get(current_word,{}) or current_word not in EXISTING_WORDS):
                del wordDict[current_word]
            current_word = elem.text.lower()
            root.clear()

            if ':' in current_word:
                skipThisWord = True
                continue
            skipThisWord = False
            if current_word not in wordDict:
                wordDict[current_word] = {}
        if skipThisWord:
            continue
        if event == "end" and elem.tag.endswith( "text"):
            if elem.text != 'aardvark' and not wordDict: #skip errything until the first word
              continue
            wordDict[current_word] = parsePronunciations(getDescChunk(elem.text, PRONUNCIATION))
#            wordDict[current_word]['pronunciation'] = parsePronunciations(getDescChunk(elem.text, PRONUNCIATION))
#            wordDict[current_word]['synonyms'] = parseSynonyms(getDescChunk(elem.text, SYNONYMS))
#            wordDict[current_word]['definitions'] = getAllDefinitions(elem.text)
#            wordDict[current_word]['puns'] = []
            if current_word == 'the':
                pass
            root.clear()

        if currRow>numRows:
            break
    return wordDict


def wordDictWithPronunciationsOnly(wordDict):
    retDict = copy.deepcopy(wordDict)
    for word in wordDict:
        if not wordDict[word]:
            del retDict[word]
    return retDict


def findSimilarSoundingWords(wordDict):
    similarSoundTuples = []
    wordsWithPronunciations = wordDict
    d1 = copy.deepcopy(wordsWithPronunciations)
    d2 = copy.deepcopy(wordsWithPronunciations)
    for index, word1 in enumerate(d1):
        word1Pronunciations = wordDict[word1]
        if not word1Pronunciations:continue
        for word2 in d2:
            if word1 == word2:continue
            word2Pronunciations = wordDict[word2]
            if not word2Pronunciations:continue
            for pron1 in word1Pronunciations:
                for pron2 in word2Pronunciations:
                    if pron1 == pron2 and len(pron1)>0:
                        bothWords = ':'.join(sorted([word1, word2]))
                        similarSoundTuples.append(bothWords)
    return set(similarSoundTuples)


def printOutput(wordDict, similarSoundingWords):
    wordTuples = [word.split(':') for word in similarSoundingWords]
    for allWords in wordTuples:
        print '------'
        print allWords
        for word in allWords:
            print word
            print wordDict[word]['pronunciation']
            for definition in wordDict[word]['definitions']:
                print '* ' + definition


def updateWordDictWithPuns(wordDict, similarSoundingWords):
    retDict = {}
    for words in similarSoundingWords:
        word1, word2 = words.split(':')
        print word1, word2
        if word1 not in retDict:
            retDict[word1] = []
        if word2 not in retDict:
            retDict[word2] = []
        retDict[word1].append(word2)
        retDict[word2].append(word1)
    return retDict

def dictToFile(wordDict):
    with open('dsh.py', 'w') as f:
        f.write('dictionary = ' + str(wordDict))


SENTENCE = 'I took my aardvark to the store'
if __name__ == '__main__':
    wordDict = getRawData()
    similarSoundingWords = findSimilarSoundingWords(wordDict)
    wordDict = updateWordDictWithPuns(wordDict, similarSoundingWords)
    print 'puns dict', wordDict
    dictToFile(wordDict)
    TIME_SINCE_LAST_UPDATE = now()
    print 'Finding similar words: ', now() - TIME_SINCE_LAST_UPDATE
    #printOutput(wordDict, similarSoundingWords)

#!/usr/bin/env python

from proDict import proDict as pd

comparedWord = 'for'

firstFewSounds = lambda word : pd[word][:3]

def wordsThatStartWith(sound):

  for word in pd:
    if firstFewSounds(word) == sound:
      print(word)

#wordsThatStartWith(firstFewSounds(comparedWord))

class WordMatcher:

    def __init__(self, word, accuracy, pronunciation=None):
      self.word = word
      self.pronunciation = pronunciation
      if not pronunciation:
        self.pronunciation = pd[word]
      self.accuracy = accuracy

    def removeNumbersFromWord(self, w):
      return self.removeNumbers(self.joinFirstLetterSound(w))

    def removeNumbers(self, s):
      retS = s
      for num in range(10):
        retS = retS.replace(str(num),'')
      return retS

    def joinFirstLetterSound(self, w):
      retV = ''
      pronunciation = pd.get(w)
      if self.word == w:
        pronunciation = self.pronunciation
      for sound in pronunciation:
        sluredSound = sound
        if len(sound) > 1:
          sluredSound = sound[0]
          if self.accuracy == 1:
            sluredSound = sound[:-1]
          if sound in ['SH','TH','CH']:
            sluredSound = sound
          if sound == 'TH' and self.accuracy == 0:
            sluredSound = sound[0]
        retV += sluredSound
      return retV

    def multiWordContain(self, splitNumber):

      sound1 = self.joinFirstLetterSound(self.word)[:splitNumber]
      sound2 = self.joinFirstLetterSound(self.word)[splitNumber:]
      print(sound1, sound2)
      startings = []
      endings = []
      for word1 in pd:
        if self.joinFirstLetterSound(word1).endswith(sound1):
          startings.append( word1)
        if self.joinFirstLetterSound(word1).startswith(sound2):
          endings.append( word1)
      if not startings or not endings:
        return '---'
      print(startings)
      print(endings)

    def wordsThatContainWord(self, word):

      sound = pd[word]
      for word1 in pd:
        if ''.join(sound) in ''.join(pd[word1]):
          print(word1)

    def printAllCombosOfWord(self):
      for num in range(2,len(self.joinFirstLetterSound(self.word))-1):
        self.multiWordContain(num)

if __name__ == '__main__':
  import sys
  word = sys.argv[2]
  accuracy = int(sys.argv[1])
  pronunciation = pd[word]
  #if len(sys.argv) > 3:
    #pronunciation = list(['S', 'AE1', 'T','Y', 'EH1', 'N'])
  print(pronunciation)
  wm = WordMatcher(word, accuracy, pronunciation=pronunciation)
  wm.printAllCombosOfWord()

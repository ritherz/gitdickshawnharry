#!/usr/bin/env python

import sys
from collections import Counter

from dictionary import flatten
from symbols import symbols
from alliterate import SUGGESTABLE_WORDS

NUMBER_OF_CONNECTING_SOUNDS = 3
NUMBER_OF_RHYMES_TO_DISPLAY = 5
SYNONYM_DEPTH = 2

def wordEnding(word, defaultWord=None, numSounds=7):
  if not proDict.get(word): 
    if defaultWord is not None and word[-numSounds:] == defaultWord[-numSounds:]:
      print(word, defaultWord, 'Found potential word sound replacement')
    return None
  return ' '.join(proDict.get(word)[-NUMBER_OF_CONNECTING_SOUNDS:])

def wordStarting(word, defaultWord=None):
  if not proDict.get(word) :
    if not proDict.get(defaultWord): return None
    return ' '.join(proDict.get(defaultWord)[:1])
  return ' '.join(proDict.get(word)[:1])



class WordFinder:

  def __init__(self):
    self.generateRhymeEndings()

  def generateRhymeEndings(self):
    self.rhymes = []
    #NEED TO ADD OR REMOVE FOR LOOPS FOR EACH CONNECTING SOUND
    if NUMBER_OF_CONNECTING_SOUNDS == 3:
      for symbol in symbols:
        for s2 in symbols:
          for s3 in symbols:
            self.rhymes.append(symbol + ' ' + s2 + ' ' + s3)


  def letterMatch(self, word, syn):
    return syn.startswith(word[0]) and self.matchingScheme == wordStarting


  def findSimilarSoundingWords(self, sentence):
    potentialWords = {}
    for word in sentence.split(' '):
      startSynonyms = findSynonymsForWord(word)
      synonyms = list(startSynonyms)
      if SYNONYM_DEPTH == 2:
        for s in startSynonyms:
          synonyms.append(list( findSynonymsForWord(s)))
        synonyms = flatten(synonyms)
      synonyms = set(synonyms)
      potentialWords[word] = {}
      wordScheme = self.matchingScheme(word)
      for syn in synonyms:
        useWordPronunciation = False
        if not proDict.get(syn): 
          if self.letterMatch(word, syn):
            useWordPronunciation = True
            print 'potential match', word, syn
          else:
            continue #REPLACE WITH FINDING BASED ON THE LETTERS IF DOESNT EXIST

        pronunciation = self.matchingScheme(syn, word)
        if not potentialWords[word].get(pronunciation): potentialWords[word][pronunciation] = []
        potentialWords[word][pronunciation].append(syn)
    return potentialWords


  def findRhymes(self, sentence, targetWord, matchingScheme):
    self.matchingScheme = matchingScheme
    potentialWords = self.findSimilarSoundingWords(sentence)
    rhymeTotals = {}
    if self.matchingScheme == wordStarting:
      for symbol in symbols:
        for word in sentence.split(' '):
          if potentialWords[word].get(symbol):
            if not rhymeTotals.get(symbol): rhymeTotals[symbol] = 0
            rhymeTotals[symbol] += 1

    if self.matchingScheme == wordEnding:
      for rhyme in self.rhymes:
        for word in sentence.split(' '):
          if potentialWords[word].get(rhyme):
            if not rhymeTotals.get(rhyme): rhymeTotals[rhyme] = 0
            rhymeTotals[rhyme] += 1

    self.reportRhymes(rhymeTotals, sentence, targetWord)



  def reportRhymes(self, rhymeTotals, sentence, targetWord):
    if targetWord:
      sound = self.matchingScheme(targetWord)
      print(sound)

      print '----------------'
      print sound, rhymeTotals.get(sound)
      self.printRhymeSuggestions(sentence, sound)

      print 'SUGGESTIONS:'
      for word in SUGGESTABLE_WORDS:
        self.printSynonymsForSound(word, sound, dontPrintEmptyWords=True)

    else:
      self.printVariousSounds(rhymeTotals, sentence)

  def printVariousSounds(self, rhymeTotals, sentence):
    for sound, number in Counter(rhymeTotals).most_common(NUMBER_OF_RHYMES_TO_DISPLAY):
      print '----------------sounds'
      print sound, number
      self.printRhymeSuggestions(sentence, sound)

      print 'SUGGESTIONS:'
      for word in SUGGESTABLE_WORDS:
        self.printSynonymsForSound(word, sound, dontPrintEmptyWords=True)

  def printRhymeSuggestions(self, sentence, sound):
      for word in sentence.split(' '):
        self.printSynonymsForSound(word, sound)

  def printSynonymsForSound(self, word, sound, dontPrintEmptyWords=False):
    alliteratorySynonyms = []
    startSynonyms = findSynonymsForWord(word)
    synonyms = list(startSynonyms)
    if SYNONYM_DEPTH == 2:
      for s in startSynonyms:
        synonyms.append(list( findSynonymsForWord(s)))
      synonyms = flatten(synonyms)
    synonyms = set(synonyms)

    for synonym in synonyms:
      if self.matchingScheme(synonym) == sound:
        alliteratorySynonyms.append(synonym)
    if not dontPrintEmptyWords or alliteratorySynonyms:
      print word, alliteratorySynonyms


def parseArgs(args):
  inputSentence = ' '.join(args[1:])
  targetWord = None
  if inputSentence.startswith('-'):
    targetWord = args[1].strip('-')
    inputSentence = ' '.join(args[2:])
  return inputSentence, targetWord


if __name__ == '__main__':
    from proDict import proDict
    from synonyms2 import *
    inputSentence, targetWord = parseArgs(sys.argv)
    matchingScheme = wordStarting
    WordFinder().findRhymes(inputSentence, targetWord, matchingScheme)

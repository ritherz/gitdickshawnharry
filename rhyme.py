#!/usr/bin/env python

import sys
from collections import Counter

from dictionary import flatten
from symbols import symbols
from alliterate import SUGGESTABLE_WORDS

TARGET_SOUND = None

NUMBER_OF_CONNECTING_SOUNDS = 3
NUMBER_OF_RHYMES_TO_DISPLAY = 5
SYNONYM_DEPTH = 2

def findSynonymsForWordOld(word):
    word = word.rstrip().rstrip(',').rstrip('.')
    for wordVariation in [word,
                          word.rstrip('s'),
                          word.rstrip('es'),
                          word.rstrip("'s")]:
        if wordVariation in thesaurus:
            return list(set([w for w in flatten(thesaurus.get(wordVariation,[]))] + [word]))
    return []


def findSynonymsForWord(word, depth=SYNONYM_DEPTH):
    word = word.rstrip().rstrip(',').rstrip('.')
    for wordVariation in [word,
                          word.rstrip('s'),
                          word.rstrip('es'),
                          word.rstrip("'s")]:
        if wordVariation in thesaurus:
            synonyms = [w for w in flatten(thesaurus.get(wordVariation,[]))]
            if depth >= 2:
                synonyms += [findSynonymsForWord(w, depth - 1) for w in synonyms]
            synonyms = list(flatten(synonyms))
            return list(set(synonyms + [word]))
    return []


def wordEnding(word):
  if not proDict.get(word): return None
  return ' '.join(proDict.get(word)[-NUMBER_OF_CONNECTING_SOUNDS:])


def findRhymes(sentence):
  potentialWords = {}
  for word in sentence.split(' '):
    synonyms = findSynonymsForWord(word)
    potentialWords[word] = {}
    for syn in synonyms:
      if not proDict.get(syn): continue

      synEnd = wordEnding(syn)
      if not potentialWords[word].get(synEnd): potentialWords[word][synEnd] = []
      potentialWords[word][synEnd].append(syn)

  rhymes = []
  #NUMBER_OF_CONNECTING_SOUNDS should be the number of these loops
  if NUMBER_OF_CONNECTING_SOUNDS == 2:
    for symbol in symbols:
      for s2 in symbols:
        rhymes.append(symbol + ' ' + s2)
  if NUMBER_OF_CONNECTING_SOUNDS == 3:
    for symbol in symbols:
      for s2 in symbols:
        for s3 in symbols:
          rhymes.append(symbol + ' ' + s2 + ' ' + s3)
  if NUMBER_OF_CONNECTING_SOUNDS == 4:
    for symbol in symbols:
      for s2 in symbols:
        for s3 in symbols:
          for s4 in symbols:
            rhymes.append(symbol + ' ' + s2 + ' ' + s3 + ' ' + s4)



  rhymeTotals = {}
  for rhyme in rhymes:
    for word in sentence.split(' '):
      if potentialWords[word].get(rhyme):
        if not rhymeTotals.get(rhyme): rhymeTotals[rhyme] = 0
        rhymeTotals[rhyme] += 1

  reportRhymes(rhymeTotals, sentence)



def reportRhymes(rhymeTotals, sentence):
  if TARGET_WORD:
    sound = wordEnding(TARGET_WORD)

    print '----------------'
    print sound, rhymeTotals.get(sound)
    printRhymeSuggestions(sentence, sound)

    print 'SUGGESTIONS:'
    for word in SUGGESTABLE_WORDS:
      printSynonymsForSound(word, sound, dontPrintEmptyWords=True)

  else:
    printVariousSounds(rhymeTotals, sentence)

def printVariousSounds(rhymeTotals, sentence):
  for sound, number in Counter(rhymeTotals).most_common(NUMBER_OF_RHYMES_TO_DISPLAY):
    print '----------------sounds'
    print sound, number
    printRhymeSuggestions(sentence, sound)

    print 'SUGGESTIONS:'
    for word in SUGGESTABLE_WORDS:
      printSynonymsForSound(word, sound, dontPrintEmptyWords=True)

def printRhymeSuggestions(sentence, sound):
    for word in sentence.split(' '):
        printSynonymsForSound(word, sound)

def printSynonymsForSound(word, sound, dontPrintEmptyWords=False):
  alliteratorySynonyms = []
  for synonym in findSynonymsForWord(word):
    if wordEnding(synonym) == sound:
      alliteratorySynonyms.append(synonym)
  if not dontPrintEmptyWords or alliteratorySynonyms:
    print word, alliteratorySynonyms




TARGET_WORD = 'drawers'
SENTENCE = 'tycoon scoop entrepreneur shop shoppe market sweet candy tasty confection ice cream station'

if __name__ == '__main__':
    from proDict import proDict
    from thesaurus import thesaurus
    if len(sys.argv) > 1:
        inputSentence = ' '.join(sys.argv[1:])
        if inputSentence.startswith('-'):
            TARGET_SOUND = inputSentence[1]
            inputSentence = ' '.join(sys.argv[2:])
        findRhymes(inputSentence)
    else:
        findRhymes(SENTENCE)


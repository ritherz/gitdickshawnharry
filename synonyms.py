
from dictionary import flatten
from thesaurus import thesaurus
from goodDefsDict import dictionary
from collections import Counter
import sys

def findSynonymsForWord(word):
    word = word.rstrip().rstrip(',').rstrip('.')
    for wordVariation in [word,
                          word.rstrip('s'),
                          word.rstrip('es'),
                          word.rstrip("'s")]:
        if wordVariation in thesaurus:
            return [w for w in flatten(thesaurus.get(wordVariation,[]))]
    return []

if __name__ == '__main__':
    if len(sys.argv) > 1:
        print findSynonymsForWord(sys.argv[1])
    else:
        print 'What word?'


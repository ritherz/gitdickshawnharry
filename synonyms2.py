
from dictionary import flatten
import sys
sys.path.append('wordNet')
from thesaurus import thesaurus
from thesaurus2 import thesaurus as thesaurus2
from goodDefsDict import dictionary
from collections import Counter
import sys

AFFIXES = ['s','es',"'s",'ing','ly']
PREFIXES = []

suffixes = lambda word : [word + affix for affix in AFFIXES]
prefixes = lambda word : [word + prefix for prefix in PREFIXES]
wordVariations = lambda word : [word] + suffixes(word) + prefixes(word)


def findSynonymsForWord(word):
    word = word.rstrip().rstrip(',').rstrip('.')
    for wordVariation in wordVariations(word):
        if wordVariation in thesaurus or wordVariation in thesaurus2:
            return set([w for w in flatten(thesaurus.get(wordVariation,[]))] + [w for w in flatten(thesaurus2.get(wordVariation,[]))])
    return []


if __name__ == '__main__':
    if len(sys.argv) > 1:
        print findSynonymsForWord(sys.argv[1])
        print findSynonymsForWord(sys.argv[1])
    else:
        print 'What word?'



import glob

DICTIONARY_PATH = 'goodDefDict/*.csv'
GOOD_DEFINITIONS_FILE = 'goodDefsDict.py'

def parseDefs(csvRaw):
    d = {}
    for line in csvRaw.split('\r\n'):
        if not line: continue
        word = line.split(' (')[0].lstrip('"').lower()
        definition = ' '.join(line.split(' (')[1:]).rstrip('"')
        if word not in d:
            d[word] = []
        d[word].append(definition)
        if len(d[word]) > 1:
            print word
    return d


def parseDefinitionsDict():
    entireDict = {}
    for file in glob.glob(DICTIONARY_PATH):
        with open(file, 'r') as f:
            entireDict.update(parseDefs(f.read()))
    return entireDict

def writeToFile(d):
    with open(GOOD_DEFINITIONS_FILE, 'w') as f:
        f.write('dictionary = ' + str(d))

if __name__ == '__main__':
    writeToFile(parseDefinitionsDict())
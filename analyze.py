from dictionary import PUNABLE_TYPES, DEFINITION_TYPES

def numberOfDefinitions(wordDict, word):
    if word not in wordDict or 'definitions' not in wordDict[word]: return 0
    total = 0
    for definitionType in PUNABLE_TYPES:
        total += len(wordDict[word]['definitions'].get(definitionType, []))
    return total

def findPunableWords(wordDict, sentence):
    for word in sentence.split(' '):
        if numberOfDefinitions(wordDict, word) > 1:
            printDefinitions(wordDict, word)

def printDefinitions(wordDict, word):
    print '--'*30
    print word
    for definitionType in DEFINITION_TYPES:
        for definition in wordDict.get(word, {}).get('definitions', {}).get(definitionType):
            print word, definitionType, definition

SENTENCE = "aardvark come on the roof"
if __name__ == '__main__':
    import goodDefsDict
    print goodDefsDict.dictionary.get('cat')
#    with open('dsh.py','r') as f:
#        d = eval(f.read())
#    print findPunableWords(d, SENTENCE)


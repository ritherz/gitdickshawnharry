#!/usr/bin/env python3

import re
import itertools
import copy

import time

now = lambda *args : time.time()

flatten = lambda args : list(itertools.chain.from_iterable(args))

start = now()

THESAURUS_OUTPUT = 'thesaurus2.py'

def saveToFile(thesaurus):
    with open(THESAURUS_OUTPUT, 'w') as f:
        f.write('thesaurus = ' + str(thesaurus))


class WordFinder:

  def __init__(self):
    self.allFiles = ['data.noun',
                     'data.verb',
                     'data.adj',
                     'data.adv']
#    self.allFilesRaw = [f.read() for f in self.allFiles]

  def findPointer(self, pointer):
    defs = []
    for f in self.allFiles:
      f.seek(0)
      for line in f:
        if line.startswith(str(pointer)):
          defs.append(line)
    return defs

  def findWord(self, word):
    defs = []
    for f in self.allFiles:
      f.seek(0)
      for line in f:
        if re.findall('^\d+ \d+ . \d+ {0} '.format(word), line):
          defs.append(line.replace('\n',''))
    return defs

  def findSynonyms(self, wordInfos):
    synonyms = []
    farSynonyms = []
    for d in wordInfos:
      synonyms.append(d['word_lexids'])
      wordLines = []
      for p, wordType in d['synet_pointers']:
        wordInfosInner = self.parseWordLines(self.findPointer(p))
        for wii in wordInfosInner:
          farSynonyms.append([word for word, extra in wii['word_lexids']])
    return (set([word for word, extra in flatten(synonyms)]), set(flatten(farSynonyms)))

  def wordInfo(self, word):
    wordLines = self.findWord(word)
    wordInfos = self.parseWordLines(wordLines)
    synonyms = self.findSynonyms(wordInfos)

  def parseFull(self):
    fullDict = {}
    for f in self.allFiles:
      for l in open(f, 'r'):
        if l.startswith('  '):
          continue
        wordInfos = self.parseWordLines([l])
        fullDict[wordInfos[0]['id']] = wordInfos[0]

    retDict = {}
    for i, wordInfo in enumerate(fullDict):
      print(i)
      words = [word for word, extra in fullDict[wordInfo]['word_lexids']]
      pointers = [pointer for pointer, wordType in fullDict[wordInfo]['synet_pointers']]
      for pointer in pointers:
        words2 = [word for word, extra in fullDict.get(pointer, {}).get('word_lexids',[])]
      for word in words:
        if word not in retDict: retDict[word] = []
        wordsWithoutThisWord = copy.deepcopy(words)
        if word in wordsWithoutThisWord:
          wordsWithoutThisWord.remove(word)
        words2WithoutThisWord = copy.deepcopy(words2)
        if word in words2WithoutThisWord:
          words2WithoutThisWord.remove(word)
        for syn in wordsWithoutThisWord:
          retDict[word].append(syn)
        for syn in words2WithoutThisWord:
          retDict[word].append(syn)

    return retDict


        #print([pointer for pointer, wordType in fullDict[wordInfo]['synet_pointers']])

#      print(wordInfos)
#      primarySynonyms, secondarySynonyms = self.findSynonyms(wordInfos)
#      for syn in primarySynonyms:
#        if not fullDict.get(syn): fullDict[syn] = []
#        for item in primarySynonyms:
#          fullDict[syn].append(item)
#        for item in secondarySynonyms:
#          fullDict[syn].append(item)


  def parseWordLines(self, wordLines):
    allDicts = []
    for line in wordLines:
      #id lexname.id wordtype numwords_in_synset word lex_id_in_hex num_pointers [pointer_symbol synset_offset pos source]
      lineDict = {}
      if line.strip() == '': continue
      try:
        lineDict['id'], lineDict['lexname_id'], lineDict['wordType'], lineDict['numWordsInSynset'] = line.split(' ')[:4]
        wordLexIndex = 0
        word_lex_id = []
        word_lexids = []
        while not re.match('^\d\d\d$', line.split(' ')[4:][wordLexIndex]):
          word_lex_id.append(line.split(' ')[4:][wordLexIndex])
          if wordLexIndex % 2 == 1:
            word_lexids.append(word_lex_id)
            word_lex_id = []
          wordLexIndex += 1
        lineDict['word_lexids'] = word_lexids
        numPointers = int(line.split(' ')[4+wordLexIndex:][0])
        baseIndex = 4+wordLexIndex+1
        pointers = []
        try:
          for pointerIndex in range(numPointers):
            pointer_symbol, pointer, wordType, source_target_mumbojumbo = line.split(' ')[baseIndex+(pointerIndex*4):baseIndex+((pointerIndex+1)*4)]
            pointers.append((pointer, wordType))
        except:
          pass # this could be fixed if i cared
      except:
        print(line)
        print(line.split(' ')[baseIndex:])
        raise
      lineDict['synet_pointers'] = pointers
      allDicts.append(lineDict)
    return allDicts

if __name__ == '__main__':
  saveToFile(WordFinder().parseFull())
 # wf = WordFinder()
 # print(wf.findPointer('01779986'))
 # print(wf.findPointer('01779986'))
 # print(wf.findPointer('01779986'))



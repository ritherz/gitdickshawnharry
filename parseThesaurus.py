
THESAURUS_PATH = 'thesaurus/mobythes.aur'
MYTHESAURUS_PATH = 'thesaurus/th_en_US_new.dat'
THESAURUS_OUTPUT = 'thesaurus.py'
def parseMobyThesaurus():
    thes = {}
    with open(THESAURUS_PATH, 'r') as f:
        for line in f.read().split('\r'):
            word = line.split(',')[0]
            thes[word] = line.split(',')[1:]
    return thes


def filterBadSynonyms(word):
    if 'atomic number' not in w \
        and 'metallic element' not in w:
        return True
    return False

def parseMyThesaurus():
    thes = {}
    numDefinitionsCounter = 0
    word = None
    with open(MYTHESAURUS_PATH, 'r') as f:
        fileLines = f.readlines()
    print len(fileLines)
    for line in fileLines:
        if numDefinitionsCounter == 0:
            try:
                word, numDefinitionsCounter = line.split('|')
            except:

                print line
                print line.split('|')
                raise
            numDefinitionsCounter = int(numDefinitionsCounter)
            continue
        numDefinitionsCounter -= 1
        if word not in thes:
            thes[word] = []
        synonyms = [w.strip() for w in line.split('|')[1:] if 'atomic number' not in w]
        thes[word].append(synonyms)
    print len(thes)
    return thes


def saveToFile(thesaurus):
    with open(THESAURUS_OUTPUT, 'w') as f:
        f.write('thesaurus = ' + str(thesaurus))

if __name__ == '__main__':
    saveToFile(parseMyThesaurus())
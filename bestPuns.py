#!/usr/bin/env python

import proDict as pd
import freqDict as fd

for w in pd.proDict:
  if w not in fd.freqDict: continue
  count = 0
  for w2 in pd.proDict:
    if w2 not in fd.freqDict: continue
    if w != w2 and pd.proDict[w] == pd.proDict[w2]:
      print(w, w2, count)
      count += 1

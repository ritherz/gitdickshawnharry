#!/usr/bin/env python

proDict = {}
with open ('pronunciations/cmudict-0.7b', 'r') as f:
  for line in f.readlines():
    if line.startswith(';;;'):
      continue
    word, pronunciation = line.split('  ')
    pronunciations = [p.strip('\n') for p in pronunciation.split(' ')]

    proDict[word.lower()] = pronunciations


symbols = []
with open ('pronunciations/cmudict-0.7b.symbols', 'r') as f:
  for line in f.readlines():
    symbols.append( line.strip('\n') )



def writeFile(filename, data):
  with open (filename + '.py', 'w') as f:
    f.write(filename + ' = ' + str(data))

writeFile('symbols', symbols)
writeFile('proDict', proDict)

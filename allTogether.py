#!/usr/bin/env python

import sys
import rhyme
import alliterate
from thesaurus import thesaurus
from proDict import proDict
rhyme.thesaurus = thesaurus
rhyme.proDict = proDict
rhyme.NUM_ALLITERATION_SUGGESTIONS = 1
rhyme.SYNONYM_DEPTH = 1
alliterate.NUMBER_OF_RHYMES_TO_DISPLAY = 2


if __name__ == '__main__':
    if len(sys.argv) > 1:
        inputSentence = ' '.join(sys.argv[1:])
        if inputSentence.startswith('-'):
            firstWord = inputSentence.split(' ')[:1][0].replace('-','')
            if len(firstWord) == 1:
                alliterate.TARGET_LETTER = inputSentence[1]
            inputSentence = ' '.join(sys.argv[2:])
        alliterate.findAlliteration(inputSentence)
        rhyme.findRhymes(inputSentence)
    else:
        print 'Find args'


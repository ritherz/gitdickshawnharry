
from dictionary import flatten
from thesaurus import thesaurus
from goodDefsDict import dictionary
from collections import Counter
import sys
from freqList import freqList

ALPHABET = 'abcdefghijklmnopqrstuvwxyz'
SIMILAR_FIRST_LETTER_TUPLES = [('f', 'ph'), ('c', 'k')]
SUGGESTABLE_WORDS = ['like', 'good', 'bad', 'happy', 'sad', 'friend', 'more', 'very', 'better', 'do', 'most', 'such', 'other',
                     'smart', 'serious', 'positive', 'masterful','excited', 'brave', 'beautiful', 'really', 'long', 'fairly', 'extremely']
NUM_ALLITERATION_SUGGESTIONS = 2
#ALSO HAVE ALL MOST COMMON ENGLISH WORDS ABOVE

def findSynonymsForWord(word):
    word = word.rstrip().rstrip(',').rstrip('.')
    for wordVariation in [word,
                          word.rstrip('s'),
                          word.rstrip('es'),
                          word.rstrip("'s")]:
        if wordVariation in thesaurus:
            return [w for w in flatten(thesaurus.get(wordVariation,[]))]
    return []

def findAlliteration(sentence):
    countPotentionalAlliterations = {}
    for word in sentence.split(' '):
        firstLettersOfSynonyms = [w[0].lower() for w in findSynonymsForWord(word)]
        if word not in countPotentionalAlliterations:
            countPotentionalAlliterations[word] = {}
        for letter in ALPHABET:
            countPotentionalAlliterations[word][letter] = firstLettersOfSynonyms.count(letter)

    alphabetCounts = {k:{} for k in sentence.split(' ')}
    for letter in ALPHABET:
        for word in sentence.split(' '):
            alphabetCounts[word][word[0]] = 1
            if countPotentionalAlliterations.get(word,{}).get(letter,0) > 0:
                alphabetCounts[word][letter] = 1

    topLetters = {}
    for letter in ALPHABET:
        topLetters[letter] = 0
        for word in sentence.split(' '):
            if alphabetCounts.get(word).get(letter):
                topLetters[letter] += 1

#    print dict(sorted(topLetters.iteritems(), key=topLetters.get, reverse=True)[:5])
    if TARGET_LETTER:
        printAlliterationSuggestions(sentence, TARGET_LETTER)

    else:
        for letter, number in Counter(topLetters).most_common(NUM_ALLITERATION_SUGGESTIONS):
            print letter+ str(number) + '-'*50
            printAlliterationSuggestions(sentence, letter)


def similarSoundingStart(word, synonym):
    for l1, l2 in SIMILAR_FIRST_LETTER_TUPLES:
        if word.startswith(l1) and synonym.startswith(l2):
            return True
        if word.startswith(l2) and synonym.startswith(l1):
            return True
    return False


def printAlliterationSuggestions(sentence, letter):
    for word in sentence.split(' '):
        printSynonymsForLetter(word, letter)
    print("Most commonly used words:")
    print([word for i, word in enumerate(freqList) if word.startswith(letter)][:30])
    print("SUGGESTIONS:")
    for word in SUGGESTABLE_WORDS:
        printSynonymsForLetter(word, letter, dontPrintEmptyWords=True)

def printSynonymsForLetter(word, letter, dontPrintEmptyWords=False):
    alliteratorySynonyms = []
    for synonym in findSynonymsForWord(word):
        if synonym[0] == letter or similarSoundingStart(word, synonym):
            alliteratorySynonyms.append(synonym)
    if not dontPrintEmptyWords or alliteratorySynonyms:
        print word, alliteratorySynonyms

TARGET_LETTER = None
SENTENCE = "I on my fast speed towards made way home ride get forward anticipate the bus see you soon my lovely wife i look forward to dinner"
if __name__ == '__main__':
    if len(sys.argv) > 1:
        inputSentence = ' '.join(sys.argv[1:])
        if inputSentence.startswith('-'):
            TARGET_LETTER = inputSentence[1]
            inputSentence = ' '.join(sys.argv[2:])
        findAlliteration(inputSentence)
    else:
        findAlliteration(SENTENCE)

